import matplotlib.pyplot as plt

s = { -1-1j, -2-2j, -3-3j, -4-4j, -5-5j }
c1=complex(input("Enter the translation in complex number format:"))
s1={x*c1 for x in s}
c2=complex(input("Enter the translation in complex number format:"))
s2={x+c2 for x in s1}
x = [x.real for x in s2]
y = [x.imag for x in s2]
plt.scatter(x,y, label="star",marker="*",color="red",s=50)
plt.xlabel("real axis")
plt.ylabel("imaginary axis")
plt.title("Translation and Scalling")
plt.draw()
plt.show(block=False)
