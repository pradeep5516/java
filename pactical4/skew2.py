import numpy as np
n = int(input("Enter a number for row and column :"))
M = []

for i in range(n):
    print("Enter element of ",i+1," row :")
    M.append([])
    for j in range(n):
        x = int(input())
        M[i].append(x)

print("Matrix M = \n")
for i in range(n):
    for j in range(n):
        print(M[i][j],end=" ")
    print()

MT = np.transpose(M)
print("Transpose of the matrxi M = \n")
for i in range(n):
    for j in range(n):
        print(MT[i][j],end=" ")
    print()

def symm(M,MT):
    for i in range(n):
        for j in range(n):
            if(M[i][j]!= -MT[i][j]):
                return False
    return True


if symm(M,MT):
    print("Matrix M is Skew Symmetric")
else:
    print("Matrix m is not Skew symmetric")

    
