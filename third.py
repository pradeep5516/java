import matplotlib.pyplot as plt

s = { 1+1j, 2+2j, 3+3j, 4+4j, 5+5j }
x = [x.real for x in s]
y = [x.imag for x in s]
plt.scatter(x,y, label="star",marker="*",color="purple",s=30)
plt.xlabel("real axis")
plt.ylabel("imaginary axis")
plt.title("Argand Diagram")
plt.draw()
plt.show(block=False)
