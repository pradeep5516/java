import matplotlib.pyplot as plt
s = { 1+1j, 2+2j, 3+3j, 4+4j, 5+5j }

#scaling by 1/2
s4 = [x*(1/2) for x in s]
x = [x.real for x in s4]
y = [x.imag for x in s4]
plt.scatter(x,y,color="purple")
plt.draw()
plt.show(block=False)

#scaling by 1/3
s5 = [x*(1/3) for x in s]
x1 = [x.real for x in s5]
y1= [x.imag for x in s5]
plt.scatter(x1,y1,color="red")
plt.draw()
plt.show(block=False)

#scaling by 2
s6 = [x*2 for x in s]
x2 = [x.real for x in s6]
y2= [x.imag for x in s6]
plt.scatter(x2,y2,color="green")
plt.draw()
plt.show(block=False)



