a = 4 + 3j
b = 5 - 3j
add = a+b
print("Addition of given to complex number: ",add)

#conjugate
a = 4 + 3j
conj = a.conjugate()
print("Conjugate of given number:", conj)



import matplotlib.pyplot as plt
s = { 1+1j, 2+2j, 3+3j, 4+4j, 5+5j }
x = [x.real for x in s]
y = [x.imag for x in s]
plt.scatter(x,y,color="purple")
plt.draw()
plt.show(block=False)


#rotate by 90
s1 = [x*1j for x in s]
x1 = [x.real for x in s1]
y1 = [x.imag for x in s1]
plt.scatter(x1,y1,color="pink")
plt.draw()
plt.show(block=False)

#rotate by 180
plt.show(block=False)
s2 = [x*-1 for x in s]
x2 = [x.real for x in s2]
y2 = [x.imag for x in s2]
plt.scatter(x2,y2,color="red")
plt.draw()
plt.show(block=False)


#rotate by 270
plt.show(block=False)
s3 = [x*-1j for x in s]
x3 = [x.real for x in s3]
y3 = [x.imag for x in s3]
plt.scatter(x3,y3,color="blue")
plt.draw()
plt.show(block=False)



