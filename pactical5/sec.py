import numpy as np
r = int(input("Enter the number of row of Matrix M:"))
c = int(input("Enter the number of column of matrix M:"))

M = []
for i in range(r):
    print("Enter element if ", i + 1, "row :")
    M.append([])
    for j in range(c):
        n = int(input())
        M[i].append(n)

print("Matrix M=\n")
for i in range(r):
    for j in range(c):
        print(M[i][j], end= " ")

v = []
print("Enter the element in vetor:")
for i in range(c):
    x = int(input())
    v.append(x)

print("Vector element of v=:", v)
dt = np.dot(v,M)
print("Matrix vector multiplication is ", dt )
