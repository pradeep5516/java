import numpy as np
r = int(input("Enter the number of row of matrix M: "))
c = int(input("Enter the number of column of matrix M: "))

M = []
for i in range(r):
    print("Enter element of",i+1,"row")
    M.append([])
    for j in range(c):
        n=int(input("Enter the element"))
        M[i].append(n)

print("Matrix M=")
for i in range(r):
    ri=M[i:i+1]
    print("Row ",i+1,"=",ri)

for i in range(c):
    ci = [row[i] for row in M]
    print("Column ",i+1,"=",ci)

a = int(input("Enter the scaler value="))
def scalarProductMatrix(scm, a):
    for i in range(r):
        for j in range(c):
            scm[i][j] = a*M[i][j]

    print("Scalar multiplication of matrix M=")
    for i in range(r):
        for j in range(c):
            print(scm[i][j],end=" ")
        print()

scm = scalarProductMatrix(M, a)
MT = np.transpose(M)
print("Transpose of matrix M=")
for i in range(c):
    for j in range(r):
        print(MT[i][j], end=" ")
    print()


"""
Enter the number of row of matrix M: 2
Enter the number of column of matrix M: 3
Enter element of 1 row
Enter the element4
Enter the element5
Enter the element7
Enter element of 2 row
Enter the element8
Enter the element2
Enter the element7
Matrix M=
Row  1 = [[4, 5, 7]]
Row  2 = [[8, 2, 7]]
Column  1 = [4, 8]
Column  2 = [5, 2]
Column  3 = [7, 7]
Enter the scaler value=2
Scalar multiplication of matrix M=
8 10 14 
16 4 14 
Transpose of matrix M=
8 16 
10 4 
14 14 

"""
        
