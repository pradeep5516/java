import numpy as np
M= np.array([[1,2,3],
                     [0,3,-2],
                     [1,6,-1]])

print("Matrix M=",M)

r1 = M[0:1]
print("First row of matrix M=",r1)

r2 = M[1:2]
print("Second row of matrix M=",r2)

r3 = M[2:3]
print("third row of matrix M=",r3)

c1 = [row[0] for row in M]
print("First column of matrix M=",c1)

c2 = [row[1] for row in M]
print("second column of matrix M=",c2)

c3 = [row[2] for row in M]
print("third column of matrix M=",c3)

a = -5
scm = a*M
print("Scalar multiplication of matrix M=\n",scm)

MT = np.transpose(M)
print("Transpose of matrix M=\n",MT)

"""
Matrix M= [[ 1  2  3]
 [ 0  3 -2]
 [ 1  6 -1]]
First row of matrix M= [[1 2 3]]
Second row of matrix M= [[ 0  3 -2]]
third row of matrix M= [[ 1  6 -1]]
First column of matrix M= [1, 0, 1]
second column of matrix M= [2, 3, 6]
third column of matrix M= [3, -2, -1]
Scalar multiplication of matrix M=
 [[ -5 -10 -15]
 [  0 -15  10]
 [ -5 -30   5]]
Transpose of matrix M=
 [[ 1  0  1]
 [ 2  3  6]
 [ 3 -2 -1]]
"""
